import React, { Component } from 'react';
import firebase from 'firebase';
import SingleQuote from './SingleQuote';

const config = {
    apiKey: 'AIzaSyAopy0zFacAK4PFZ8Exh-LybpBbvfaheWk',
    authDomain: 'recipebook-66fc2.firebaseapp.com',
    databaseURL: 'https://recipebook-66fc2.firebaseio.com'
};

firebase.initializeApp(config);


// https://recipebook-66fc2.firebaseio.com/veon-quotes.json
const quotesRef = firebase.database().ref('veon-quotes');

class App extends Component {


    constructor() {
        super();

        this.state = {
            quoteText: '',
            quotes: {}
        };

        this.addQuote = this.addQuote.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    componentDidMount() {
        quotesRef.on('value', snapshot => {
            const val = snapshot.val();
            if (val) {
                console.log(Object.keys(val).length)
            }
            this.setState({quotes: val || {}})
        });
    }

    onInputChange(e) {
        this.setState({quoteText: e.target.value});
    }

    getProgress() {
        return (Object.keys(this.state.quotes || {}).length * 10);
    }

    getProgressStyle() {

        let width = this.getProgress() + '%';
        return {width};
    }

    removeQuote(id) {
        quotesRef.child(id).remove();
    }

    addQuote(e) {
        e.preventDefault();
        if(Object.keys(this.state.quotes).length < 10){
            quotesRef.push(this.state.quoteText, () => this.setState({quoteText: ''}));
        }else{
            alert('Для добавления новых цитат удалите одну из добавленных');
        }

    }


    renderQuotes() {
        const quotes = Object.keys(this.state.quotes)
            .map(key =>
                (<SingleQuote
                    text={this.state.quotes[key]}
                    key={key}
                    remove = {()=>this.removeQuote(key)}
                />));


        return (
            <div className="col-sm-12" style={styles.quotesList}>
                {quotes}
            </div>
        )
    }


    render() {
        return (
            <div className="container">
                <h4><b>Quotes added</b></h4>
                <div className="progress">
                    <div className="progress-bar" role="progressbar" aria-valuenow={this.getProgress()}
                         aria-valuemin="0" aria-valuemax="100" style={this.getProgressStyle()}>
                        {Object.keys(this.state.quotes || {}).length}/10
                    </div>
                </div>


                <form className="col-sm-offset-2 col-sm-8" style={styles.quoteInput}>
                    <b>Quote</b>
                    <textarea className="form-control"
                              rows="5"
                              onChange={this.onInputChange}
                              value={this.state.quoteText}
                    />
                    <button
                        className="btn btn-primary"
                        style={styles.addQuoteButton}
                        onClick={this.addQuote}
                    >
                        Add Quote
                    </button>
                </form>

                {this.renderQuotes()}
            </div>



        );
    }
}

const styles = {
    quotesList: {
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: 10,
        height: 100,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    addQuoteButton: {
        alignSelf: 'center',
        marginTop: 10
    },
    quoteInput: {
        display: 'flex',
        flexDirection: 'column'
    }
};

export default App;
