import React, { Component } from 'react';

export default class SingleQuote extends Component {

    constructor(props){
        super(props);
        this.state={
            isMouseInside: false
        }
    }

    mouseEnter = () => {
        this.setState({ isMouseInside: true });
    };

    onMouseLeave = () => {
        this.setState({ isMouseInside: false });
    };

    renderCloseButton(){
        return (
            <button
                className="btn"
                style={styles.close}
                onClick={this.props.remove}
            >
                <i className="glyphicon glyphicon-remove"/>
            </button>
        )
    }

    render(){
        return (
            <div
                onMouseEnter={this.mouseEnter} onMouseLeave={this.onMouseLeave}
                key={this.props.key}
                style={styles.quote}
            >
                {this.props.text}

                {this.state.isMouseInside ? this.renderCloseButton() : null}
            </div>
        )
    }

}

const styles={
    quote:{
        borderRadius: 5,
        border: '1px solid #DADADA',
        width: 240,
        height: 'auto',
        margin: 5,
        padding: '10px 10px',
        position:'relative',
        fontFamily: 'Arizonia',
        fontSize:20

},
    close:{

        width: 30,
        height: 30,
        textAlign: 'center',
        padding: '6px 0',
        fontSize: 12,
        lineHeight: 1.428571429,
        borderRadius:15,
        position:'absolute',
        top:-5,
        right:-5
    }
};

